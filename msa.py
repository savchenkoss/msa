#!/usr/bin/env python

import argparse
import subprocess
from collections import defaultdict
import numpy as np
from matplotlib import pyplot as plt
from libs.regions import parse_reg_file


def ismono(r):
    return np.all(np.diff(r) > 0)


def rotate(arm_coords, i, PA):
    x, y = zip(*arm_coords)
    x = np.array(x)
    y = np.array(y)
    sinPA, cosPA = np.sin(PA), np.cos(PA)
    x1 = x*cosPA + y*sinPA
    y1 = y*cosPA - x*sinPA
    x1 /= np.cos(i)
    return np.array(list(zip(x1, y1)))


def show_fits(fits_file):
    call_string = f'ds9 {fits_file} -scale log -mode region -regions shape x point'
    subprocess.call(call_string, shell=True)


def extract_spirals():
    center_region = parse_reg_file("center.reg")[0]
    x_center = center_region.params["x"]
    y_center = center_region.params["y"]
    regions = parse_reg_file("arms.reg")
    spirals = defaultdict(list)
    for reg in regions:
        color = reg.properties["color"]
        x_spiral = reg.params["x"] - x_center
        y_spiral = reg.params["y"] - y_center
        spirals[color].append((x_spiral, y_spiral))
    return spirals


def apply_msa(arm_coords):
    good_angles = []
    for incl in np.arange(0, 90):
        for posang in np.arange(0, 180):
            deprojected_coords = rotate(arm_coords, np.radians(incl), np.radians(posang))
            dists = np.hypot(*list(zip(*deprojected_coords)))
            if ismono(dists):
                good_angles.append((incl, posang))
    return good_angles


def get_posang_statistics(posang):
    """
    Function finds the proper values of the position angle taking into account the fact that the
    part of points can be around zero and part around 180 degrees
    """
    if (min(posang) > 0) and (max(posang) < 180):
        # We can compute regular mean
        posang_mean = np.mean(posang)
        posang_std = np.std(posang)
        return posang_mean, posang_std
    posang = np.array(posang)
    posang[posang > 90] -= 180
    posang_mean = np.mean(posang)
    posang_std = np.std(posang)
    if posang_mean < 0:
        posang_mean += 180
    return posang_mean, posang_std


def main(args):
    print("Mark the galaxy center and save regions as 'center.reg' file")
    show_fits(args.image)
    print("Mark spiral arms and save regions as 'arms.reg' file")
    show_fits(args.image)
    spirals = extract_spirals()
    plt.xlim(0, 180)
    plt.ylim(0, 90)
    plt.xlabel("Position angle")
    plt.ylabel("Inclination")
    inclinations = []
    pos_angles = []
    for idx, color in enumerate(spirals.keys()):
        coords = np.array(spirals[color], dtype=float)
        good_angles = apply_msa(coords)
        incl_good, posang_good = zip(*good_angles)
        if idx == 0:
            intersection = set(good_angles)
        else:
            intersection = intersection & set(good_angles)
        inclinations.append(np.mean(incl_good))
        pos_angles.append(np.mean(posang_good))
        plt.scatter(posang_good, incl_good, color=color)
    incl_inter, posang_inter = zip(*intersection)
    if len(incl_inter) != 0:
        # The intersection is not empty
        plt.scatter(posang_inter, incl_inter, color="0.75")
        incl_mean = np.mean(incl_inter)
        incl_std = np.std(incl_inter)
        posang_mean, posang_std = get_posang_statistics(posang_inter)
    else:
        # The intersection is empty. Computing mean of each arm and then meanvalue amon all arms
        incl_mean = np.mean(inclinations)
        incl_std = np.std(inclinations)
        posang_mean = np.mean(pos_angles)
        posang_std = np.std(pos_angles)
    plt.errorbar([posang_mean], [incl_mean], xerr=[posang_std], yerr=[incl_std], color="k")
    plt.text(x=140, y=85, s=f"$i={incl_mean: 1.1f} \pm {incl_std:1.1f}$")
    plt.text(x=140, y=80, s=f"$PA={posang_mean: 1.1f} \pm {posang_std:1.1f}$")
    plt.show()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("image", help="Path to FITS file")
    args = parser.parse_args()
    main(args)
